# Unofficial Panasonic Home Theatre Remote

http://remote.host-it.co.nz

Unfortunately Panasonic receivers do not support https requests.

## Available Scripts

In the project directory, you can run:

### `npm install`

Installs the application's dependencies.

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.

## Debugging in the Editor

### Visual Studio Code

You need to have the latest version of [VS Code](https://code.visualstudio.com) and VS Code [Chrome Debugger Extension](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome) installed.

Start your app by running `npm start`, and start debugging in VS Code by pressing `F5` or by clicking the green debug icon.

You can now write code, set breakpoints, make changes to the code, and debug your newly modified code—all from your editor.
