import React from "react";
import ReactDOM from "react-dom";
import "minireset.css";

import App from "./App";

import "./index.css";

ReactDOM.render(
  <App
    style={
      {
        "--primary-animation-speed": "0.1s",
        "--primary-color-rgb": "0, 0, 0",

        "--border-radius": "3px",

        "--content-size": "var(--line-height)",
        "--gap-size": "calc((var(--size) - var(--content-size)) / 2)",
        "--line-height": "1.75rem",
        "--size": "3rem",

        "--focus-box-shadow": "0 0 0 2px",
        "--focus-box-shadow-opacity": "0.3",
      } as React.CSSProperties
    }
  />,
  document.getElementById("root")
);
