interface IParameters {
  [index: string]: number;
}

async function execute(
  host: string,
  command: string,
  parameters: IParameters = {},
  cb = () => {}
) {
  try {
    await fetch(
      `http://${host}/WAN/dvdr/dvdr_NAI00${command === "EXEC" ? 6 : 3}.cgi`,
      {
        headers: {
          "User-Agent": "MEI-LAN-REMOTE-CALL"
        },
        method: "POST",
        body: `cCMD_${command}.x=100&cCMD_${command}.y=100${Object.keys(
          parameters
        ).map(parameter => `&c${parameter}=${parameters[parameter]}`)}`
      }
    );
  } catch (e) {
    cb();
  }
}

const controls = [
  "CHG_ARC",
  "CHG_AUX",
  "CHG_DIN",
  "RC_IPOD",
  "RC_MUTE",
  "RC_SLEEP",
  "RC_RELEASE",
  "EXEC"
];

export default controls.reduce(
  (acc, control) => {
    acc[control] = (host, release = true, parameters: IParameters = {}) =>
      execute(host, control, parameters, () => {
        if (release && control.startsWith("RC") && control !== "RC_RELEASE") {
          execute(host, "RC_RELEASE", {}, () => console.log(control));
        } else {
          console.log(control);
        }
      });
    return acc;
  },
  {} as {
    [index: string]: (
      host: string,
      release?: boolean,
      parameters?: IParameters
    ) => any;
  }
);
