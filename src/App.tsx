import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faChromecast,
  faPlaystation,
  faSteam,
} from "@fortawesome/free-brands-svg-icons";
import { faVolumeDown, faVolumeUp } from "@fortawesome/free-solid-svg-icons";

import commands from "./helpers/commands";

import "./components/button.css";
import "./components/icon.css";
import "./components/input.css";
import "./components/utilities.css";

interface Props {
  style: React.CSSProperties;
}

class App extends Component<Props> {
  state = {
    host: "192.168.1.11",
  };

  public render() {
    const { style } = this.props;
    return (
      <div style={style}>
        <h1>Panasonic Remote</h1>
        <label htmlFor="host">IP Address</label>
        <input
          id="host"
          onChange={this.onHostChange}
          placeholder="192.168.1.11"
          value={this.state.host}
        />
        <hr />
        <h3>Devices</h3>
        <button className="u-text-align-left" onClick={this.onClickChromecast}>
          <FontAwesomeIcon icon={faChromecast} />
          Chromecast
        </button>
        <button className="u-text-align-left" onClick={this.onClickPC}>
          <FontAwesomeIcon icon={faSteam} />
          Computer
        </button>
        <button className="u-text-align-left" onClick={this.onClickPS4}>
          <FontAwesomeIcon icon={faPlaystation} />
          PlayStation 4
        </button>
        <hr />
        <h3>Volume</h3>
        <div style={{ display: "flex", maxWidth: "500px" }}>
          <span className="icon">
            <FontAwesomeIcon icon={faVolumeDown} />
          </span>
          <button onClick={() => this.changeVolume(8)}>8</button>
          <button onClick={() => this.changeVolume(12)}>12</button>
          <button onClick={() => this.changeVolume(18)}>18</button>
          <button onClick={() => this.changeVolume(30)}>30</button>
          <button onClick={() => this.changeVolume(35)}>35</button>
          <span className="icon">
            <FontAwesomeIcon icon={faVolumeUp} />
          </span>
        </div>
        <h4>PS4</h4>
        <div style={{ display: "flex", maxWidth: "500px" }}>
          <span className="icon">
            <FontAwesomeIcon icon={faVolumeDown} />
          </span>
          <button onClick={() => this.changeVolume(40)}>40</button>
          <button onClick={() => this.changeVolume(45)}>45</button>
          <button onClick={() => this.changeVolume(50)}>50</button>
          <button onClick={() => this.changeVolume(55)}>55</button>
          <button onClick={() => this.changeVolume(60)}>60</button>
          <span className="icon">
            <FontAwesomeIcon icon={faVolumeUp} />
          </span>
        </div>
      </div>
    );
  }

  private onHostChange = (
    event: React.ChangeEvent<
      HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement
    >
  ) => {
    this.setState({ host: event.target.value });
  };

  private onClickBluetooth = () => {
    commands.RC_IPOD(this.state.host);
  };

  private onClickBluetoothPair = () => {
    commands.RC_IPOD(this.state.host, false);
    setTimeout(() => commands.RC_RELEASE(this.state.host), 7 * 1000);
  };

  private onClickChromecast = () => {
    commands.CHG_AUX(this.state.host);

    commands.EXEC(this.state.host, true, {
      VOLUME: 12,
    });
  };

  private onClickPS4 = () => {
    commands.CHG_AUX(this.state.host);

    commands.EXEC(this.state.host, true, {
      VOLUME: 50,
    });
  };

  private onClickPC = () => {
    commands.EXEC(this.state.host, true, {
      VOLUME: 12,
    });

    commands.CHG_DIN(this.state.host);
  };

  private changeVolume = (VOLUME: number) => {
    commands.EXEC(this.state.host, true, { VOLUME });
  };
}

export default App;
